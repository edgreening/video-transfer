#!/usr/bin/env python
import argparse
import ffmpeg
import pathlib

# defaults
BASE_DIR = '/Users/ed/Movies/'
DRY_RUN = False
NEW_EXT = 'mp4'
MOVE = False
TRANSFER_EXTS = ('srt', 'avi', 'mp4')
OLD_EXT = 'mkv'
VERBOSE = False
DESTINATION_DIR = '/Volumes/NO NAME/'


def convert_videos(base_dir, old_ext, new_ext, dry_run, verbose):
    """
    Convert videos found in base_dir from old_ext to new_ext.
    """
    for source in pathlib.Path(base_dir).rglob(f'*{old_ext}'):
        # create in same location
        destination = source.with_suffix(f'.{new_ext}')

        if verbose:
            print(f'Attempting to convert {source} to {new_ext}.')

        if not destination.exists():
            if not dry_run:
                ffmpeg.input(source).output(str(destination), acodec='copy', vcodec='copy').run()
            else:
                pass
            print(f'Successfully created {destination.name}.')
        else:
            print(f'Failed as {destination.name} already exists.')

        if verbose:
            print('---')


def transfer_videos(base_dir, mv_exts, new_parent, move, dry_run, verbose):
    """
    Transfer videos found in base_dir with an extension in mv_ext to new_parent.
    """
    for ext in mv_exts:

        if verbose:
            print(f'Looking for {ext} files.')

        for source in pathlib.Path(base_dir).rglob(f'*{ext}'):

            if verbose:
                print(f'Attempting to {"move" if move else "copy"} {source} to {new_parent}.')

            destination = pathlib.Path(new_parent) / source.name

            if not destination.exists():
                if not dry_run:
                    if move:
                        p.replace(destination)
                    else:
                        destination.write_bytes(source.read_bytes())
                else:
                    pass
                print(f'Successfully moved {destination.name}.',)
            else:
                print(f'Failed as {destination.name} already exists.')

            if verbose:
                print('---')
                
                
def get_args():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('dir', nargs='*', help='directories in which to search for files', type=str, metavar='dir', default=[BASE_DIR])
    parser.add_argument("-m", "--mode", help="'convert' or 'transfer' videos", choices=['convert', 'transfer'], required=True)
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true", default=VERBOSE)
    parser.add_argument("-d", "--dry-run", help="dry run of program", action="store_true", default=DRY_RUN)
    parser.add_argument("--move", help="move video rather than copy", action="store_true", default=MOVE)
    parser.add_argument("--new-ext", help="what to convert videos to", type=str, default=NEW_EXT)
    parser.add_argument("--old-ext", help="which videos to convert", type=str, default=OLD_EXT)
    parser.add_argument("--mv-exts", help="what extensions to transfer", type=str, default=TRANSFER_EXTS)
    parser.add_argument("--destination", help="where to transfer to", type=str, default=DESTINATION_DIR)

    return parser.parse_args()

                
if __name__ == '__main__':

    args = get_args()

    if args.dry_run:
        print('Running dry-run of program.')

    for base_dir in args.dir:

        if args.verbose:
            print(f'Searching for video files in {base_dir}')

        if args.mode == 'convert':
            convert_videos(base_dir=base_dir, old_ext=args.old_ext, new_ext=args.new_ext, dry_run=args.dry_run, verbose=args.verbose)
        
        else:
            transfer_videos(base_dir=base_dir, mv_exts=args.mv_exts, new_parent=args.destination, move=args.move, dry_run=args.dry_run, verbose=args.verbose)
